<html lang="en" class="">
<?php
// Mois courant passé par paramètre
if (isset($_REQUEST['month']))
{
	$current_month = (int)$_REQUEST['month'];
}
// Mois enregistré en cookie 
elseif (isset($_COOKIE['current_month']))
{
	$current_month = (int)$_COOKIE['current_month'];
}
else
{
	$current_month = date('n');
}

// Année courante passé par paramètre
if (isset($_REQUEST['year']))
{
	$current_year = (int)$_REQUEST['year'];
} 
// Annnée enregistrée en cookie 
elseif (isset($_COOKIE['current_year']))
{
	$current_year = (int)$_COOKIE['current_year'];
}
else
{
	$current_year = date('Y');
}

// Enregistrement en cookies
setcookie('current_month', $current_month, time()+60*60*24*30); // 30j en secondes 
setcookie('current_year', $current_year, time()+60*60*24*30); // 30j en secondes
?>	
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex">
	<link rel="stylesheet" type="text/css" href="calendar.css">

	<title>Calendrier</title>
</head>

<body>
<div class="wrapp">
		<div class="flex-calendar">
			
			<?php
				// Mois/année en cours			
				$this_month = strtotime($current_year . '-' . $current_month);
				
				// Mois précédent + Mois suivant
				$previous_month = date('m', strtotime('previous month', $this_month));
				$previous_year = date('Y', strtotime('previous month', $this_month));
				$next_month = date('m', strtotime('next month', $this_month));
				$next_year = date('Y', strtotime('next month', $this_month));
			?>
			
			<div class="month">
				<a href="calendar.php?year=<?php echo $previous_year ?>&month=<?php echo $previous_month ?>" class="arrow visible"></a>

				<div class="label">
					<?php echo date( 'F Y', $this_month ); ?>
				</div>

				<a href="calendar.php?year=<?php echo $next_year ?>&month=<?php echo $next_month ?>" class="arrow visible"></a>
			</div>

			<div class="week">
				<div class="day">M</div>
				<div class="day">T</div>
				<div class="day">W</div>
				<div class="day">T</div>
				<div class="day">F</div>
				<div class="day">S</div>
				<div class="day">S</div>
			</div>

			<div class="days">
				
			<?php
				
				// Bornes du mois courant
				$first_day_of_month = date('N', strtotime('first day of ' . $current_year . '-' . $current_month));
				$last_day_of_month = date('d', strtotime('last day of ' . $current_year . '-' . $current_month));
				
				$today = date('Y-n-d');
				$disabled = array();
				$events = array();
				
				// Décalage premier jour du mois
				for ($i = 1; $i < $first_day_of_month; $i++)
				{
					echo '<div class="day out"><div class="number"></div></div>';
				}
				
				// Calendrier + events dans les cases "jour"
				for($i = 1; $i <= $last_day_of_month; $i++)
				{
					$classes = 'day';
					
					if (($current_year . '-' . $current_month . '-' . $i) == $today) $classes .= ' selected';
					
					if (in_array( $i, $disabled)) $classes .= ' disabled';

					if (in_array($i, $events)) $classes .= ' event';
					
					echo '<div class="' . $classes . '"><div class="number">' . $i . '</div></div>';
				}
			?>
			
			</div>
			<!-- Sauvegarde de la Session depuis x temps -->
			<div class="SessionConnect">
			
				<?php
				session_start();
					if (! isset( $_SESSION['login_datetime']))
						$_SESSION['login_datetime'] = date( 'Y-m-d \ H:i:s');
					echo 'Vous êtes connecté depuis : ' . $_SESSION['login_datetime'];
				?>
			</div>
			<br />

			<h2>Ajoutez un évènement</h2>
			<p>Les champs avec * sont obligatoires</p>

			<form method="post" action="calendar.php" enctype="multipart/form-data">
	   			<p>
		   			<label for="date">Date* : </label>
		   			<input type="date" name="date" id="date" value="" required>
	   			</p>
				<p>
		   			<label for="nom_event">Nom* : </label>
		   			<input type="text" name="nom_event" id="nom_event" value="" required>
	   			</p>
				<p>
        			<label for="image">Image : </label>
        			<input type='file' name="image" id="image" value="">
				</p>
	   			<p>
		   			<button type="submit">ENREGISTRER DONNÉES</b>
	   			</p>
			</form>


		<!-- Sauvegarde de l'évènement -->
	   <?php
        	if (isset($_REQUEST['date']) && $_REQUEST['date'])
				echo '<p> La date que vous avez saisie est : ' . $_REQUEST['date'] . '</p>';
		
			if (isset($_REQUEST['nom_event']) && $_REQUEST['nom_event'])
				echo '<p> Votre event s\'appel : ' . $_REQUEST['nom_event'] . '</p>';

			if (isset($_FILES['image']) && $_FILES['image']['size'])
				{
					$finfo = finfo_open(FILEINFO_MIME_TYPE);
					
					if (finfo_file($finfo, $_FILES['image']['tmp_name']) == 'image/jpeg')
					{
						move_uploaded_file($_FILES['image']['tmp_name'], 'hello.jpg');
					}
				}
    	?>

		<!--Ayant eu beaucoup de difficultés pour ce devoir, je n'ai pas su afficher les images, ainsi que les evenements enregistrés via le formulaire.
		Je me suis aussi basé sur l'exemple donné en classe. Je trouve que ce langage est très interessant du fait de ces fonctionnalités lorsqu'on le lie à 
		une BDD, mais pour le moment je n'ai pas bien compris sa syntaxe, ce qui me pose bcp de problèmes. Mais ça va Changer!! -->

		</div>
	</div>
</body>
</html>

